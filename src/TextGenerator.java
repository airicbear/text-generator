import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class TextGenerator {
	HashMap<String, ArrayList<String>> map;
	String[] mapList;
	String sentence;
	String text;
	
	/**
	 * <h1>About</h1>
	 * <ul>This text generator is able to generate random sentences from the given text</ul>
	 * @param text - Filepath of the text
	 */
	public TextGenerator(String text) {
		setText(text);
		map = new HashMap<String, ArrayList<String>>();
		updateMap(textToList(this.text));
		mapList = map.keySet().toArray(new String[map.keySet().size()]);
		sentence = generateSentence();
	}
	
	/**
	* This method takes the given text file and adds the words one
	* at a time to an ArrayList and returns that list.
	* @param name � file
	* @return the list of words
	* @throws FileNotFoundException 
	*/
	public ArrayList<String> textToList(String name) {
		ArrayList<String> list = new ArrayList<String>();
		try {
			File file = new File(name);
			Scanner s = new Scanner(file);
			while (s.hasNext()){
			    list.add(s.next());
			}
			s.close();
		} catch (FileNotFoundException e) {e.printStackTrace();}
		return list;
	}
	
	/**
	* Adds every word in the text to the HashMap map except
	* the last word. The keys are the words and the values
	* are the ArrayList<String> of all the words that follow that word.
	* @param text
	*/
	public void updateMap(ArrayList<String> text) {
		for(int i = 0; i < text.size()-1; i++) {
			map.put(text.get(i), new ArrayList<String>(text.subList(i+1, i+2)));
		}
	}
	
	/**
	* Returns a string representing a sentence from the given hash map
	*/
	public String generateSentence() {
		// Establish sentence structures
		ArrayList<String> firstWord   = new ArrayList<String>();
		ArrayList<String> middleWords = new ArrayList<String>();
		ArrayList<String> lastWord    = new ArrayList<String>();
		
		// Add words to sentence structures
		for(int i = random(mapList.length); lastWord.isEmpty() && i < mapList.length; i++) {
			String s = mapList[i];
				 if ( firstWord.isEmpty() && possibleFirstWord (s)) firstWord  .add(s);
			else if (!firstWord.isEmpty() && possibleLastWord  (s)) lastWord   .add(s);
			else if (!firstWord.isEmpty() && possibleMiddleWord(s)) middleWords.add(s);
				 
			// Extra randomness
			i = random(mapList.length);
		}
		
		// Merge sentence structures into one string
		String result = "";
		for(String s : firstWord  ) result += s + " ";
		for(String s : middleWords) result += s + " ";
		for(String s : lastWord   ) result += s + " ";
		return result;
	}
	
	/**
	 * @param bound - int
	 * @return Random integer from 0 to <b>bound</b>
	 */
	private int random(int bound) {
		return new Random().nextInt(bound);
	}
	
	/**
	 * @param s - String
	 * @return True if <b>s</b> is a possible first word
	 * <ul>
	 * 	<li>First letter is capitalized</li>
	 * 	<li>Does not end in punctuation</li>
	 * 	<li>Is not a bad word</li>
	 * 	<li>Is not in all caps</li>
	 * </ul>
	 */
	private boolean possibleFirstWord(String s) {
		return isCapitalized(s) && !hasPunctuation(s) && !badWord(s);
	}

	/**
	 * @param s - String
	 * @return True if <b>s</b> is a possible middle word
	 * <ul>
	 * 	<li>First letter is not capitalized</li>
	 * 	<li>Starts with a letter</li>
	 * 	<li>Does not end with "]"</li>
	 * 	<li>Is not in all caps</li>
	 * </ul>
	 */
	private boolean possibleMiddleWord(String s) {
		return !isCapitalized(s) && startsWithLetter(s) && lastLetter(s) != ']';
	}
	
	/**
	 * @param s - String
	 * @return True if <b>s</b> is a possible last word
	 * <ul>
	 * 	<li>Starts with letter</li>
	 * 	<li>Ends in punctuation</li>
	 * 	<li>Is not a bad word</li>
	 * 	<li>Is not in all caps</li>
	 * </ul>
	 */
	private boolean possibleLastWord(String s) { 
		return startsWithLetter(s) && hasPunctuation(s) && !badWord(s);
	}
	
	/**
	 * @param s - String
	 * @return True if <b>s</b> is in all caps or contains any of the following characters: 
	 * <ul>
	 * 	<li>' - : ; , ]</li>
	 * </ul>
	 */
	private boolean badWord(String s) {
		boolean result = false;
		char[] badChars = {'\'', '-', ':', ';', ',', ']'};
		for(char c : badChars) if(s.indexOf(c) > 0) result = true;
		return result || inAllCaps(s);
	}
	
	/**
	 * @param s - String
	 * @return First letter of <b>s</b>
	 */
	private char firstLetter(String s) {
		return s.charAt(0);
	}

	/**
	 * @param s - String
	 * @return Last letter of <b>s</b>
	 */
	private char lastLetter(String s) {
		return s.charAt(s.length()-1);
	}
	
	/**
	 * @param s - String
	 * @return True if <b>s</b> is capitalized
	 */
	private boolean isCapitalized(String s) {
		return Character.isUpperCase(firstLetter(s));
	}
	
	/**
	 * @param s - String
	 * @return True if <b>s</b> starts with a letter
	 */
	private boolean startsWithLetter(String s) {
		return Character.isLetter(firstLetter(s));
	}
	
	/**
	 * @param s - String
	 * @return True if <b>s</b> ends with punctuation
	 */
	private boolean hasPunctuation(String s) {
		return !Character.isLetter(lastLetter(s));
	}
	
	/**
	 * @param s - String
	 * @return True if <b>s</b> is in all caps
	 */
	private boolean inAllCaps(String s) {
		return s == s.toUpperCase();
	}
	
	public void setText(String text) {
		this.text = text;
	}
}